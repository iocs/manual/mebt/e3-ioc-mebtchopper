Summary 
================ 
This IOC provides EPICS support for the FID Technology HV Fast Pulser Power Supply (custom design for ESS) and also for the MEBT Chopper Local Protection PLC (Siemens S7-1500 Family).


Dependencies 
============ 
* s7plc
* streamDevice

Using the module 
================ 
Load the module and dependencies:

     require s7plc
     require stream

Then load the main modules for the PLC and/or Pulsers control:

     TBD
