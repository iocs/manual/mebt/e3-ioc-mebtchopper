########################################################
# File: mebtch_plc_ai.template
# MEBT Chopper PLC - analog input records 
# Created in 10/2018  by Idoia Mazkiaran - ESS Bilbao
# Modified in 10/2020 by Joao Paulo Martins - ESS
########################################################

#- Set LOW warning threshold to PLC program
record (ao, "$(P)$(R)-Low-SP") {
  field (DESC, "$(DESC) Low Warning Setpoint")
  field (EGU,  "ºC")
  field (PREC, "1")
  field (ASLO, "0.1")
  field (PINI, "YES")
  field (DTYP, "S7plc")
  field (OUT, "@$(PLC)/$(OUTOFF)+0 T=WORD")
  info(autosaveFields_pass0, "VAL")
  info(ARCHIVE_THIS, "")
}

#- Set LOW warning threshold to PLC program
record (ao, "$(P)$(R)-High-SP") {
  field (DESC, "$(DESC) High Warning Setpoint")
  field (EGU,  "ºC")
  field (PREC, "1")
  field (ASLO, "0.1")
  field (PINI, "YES")
  field (DTYP, "S7plc")
  field (OUT, "@$(PLC)/$(OUTOFF)+2 T=WORD")
  info(autosaveFields_pass0, "VAL")
  info(ARCHIVE_THIS, "")
}

#- Set LOW ALARM threshold to PLC program
record (ao, "$(P)$(R)-Lolo-SP") {
  field (DESC, "$(DESC) Low Alarm Setpoint")
  field (EGU,  "ºC")
  field (PREC, "1")
  field (ASLO, "0.1")
  field (PINI, "YES")
  field (DTYP, "S7plc")
  field (OUT, "@$(PLC)/$(OUTOFF)+4 T=WORD")
  info(autosaveFields_pass0, "VAL")
  info(ARCHIVE_THIS, "")
}

#- Set HIGH ALARM threshold to PLC program
record (ao, "$(P)$(R)-Hihi-SP") {
  field (DESC, "$(DESC) High Alarm Setpoint")
  field (EGU,  "ºC")
  field (PREC, "1")
  field (ASLO, "0.1")
  field (PINI, "YES")
  field (DTYP, "S7plc")
  field (OUT, "@$(PLC)/$(OUTOFF)+6 T=WORD")
  info(autosaveFields_pass0, "VAL")
  info(ARCHIVE_THIS, "")
}

#- Reset alarms on PLC program
record (bo, "$(P)$(R)-Rst") {
  field (DESC, "$(DESC) Reset Alarm Cmd")
  field (DTYP, "S7plc")
  field (PINI, "NO")
  field (VAL,  "0")
  field (ZNAM, "FALSE")
  field (ONAM, "TRUE")
  field (HIGH, "2")
  field (OUT, "@$(PLC)/$(OUTOFF)+8 T=BYTE B=$(BO0=0)")
  info(ARCHIVE_THIS, "")
}

#################################################################################

#- Get LOW warning threshold from PLC program
record (ai, "$(P)$(R)-Low-RB") {
  field(DESC, "$(DESC) Low Warning Readback")
  field(EGU,  "ºC")
  field(PREC, "1")
  field(ASLO, "0.1")
  field(SCAN, "I/O Intr")
  field(DTYP, "S7plc")
  field(INP, "@$(PLC)/$(INOFF)+0 T=WORD")
  field(PINI, "YES")
  info(ARCHIVE_THIS, "")
}

#- Get HIGH warning threshold from PLC program
record (ai, "$(P)$(R)-High-RB") {
  field(DESC, "$(DESC) High Warning Readback")
  field(EGU,  "ºC")
  field(PREC, "1")
  field(ASLO, "0.1")
  field(SCAN, "I/O Intr")
  field(DTYP, "S7plc")
  field(INP, "@$(PLC)/$(INOFF)+2 T=WORD")
  field(PINI, "YES")
  info(ARCHIVE_THIS, "")
}

#- Get LOW ALARM threshold from PLC program
record (ai, "$(P)$(R)-Lolo-RB") {
  field(DESC, "$(DESC) Low Alarm Readback")
  field(EGU,  "ºC")
  field(PREC, "1")
  field(ASLO, "0.1")
  field(SCAN, "I/O Intr")
  field(DTYP, "S7plc")
  field(INP, "@$(PLC)/$(INOFF)+4 T=WORD")
  field(PINI, "YES")
  info(ARCHIVE_THIS, "")
}

#- Get HIGH ALARM threshold from PLC program
record (ai, "$(P)$(R)-Hihi-RB") {
  field(DESC, "$(DESC) High Alarm Readback")
  field(EGU,  "ºC")
  field(PREC, "1")
  field(ASLO, "0.1")
  field(SCAN, "I/O Intr")
  field(DTYP, "S7plc")
  field(INP, "@$(PLC)/$(INOFF)+6 T=WORD")
  field(PINI, "YES")
  info(ARCHIVE_THIS, "")
}

#- Get Reset read-back status from PLC program
record (bi, "$(P)$(R)-Rst-RB") {
  field (DESC, "$(DESC) Reset Readback")
  field (DTYP, "S7plc")
  field (SCAN, "I/O Intr")
  field (PINI, "YES")
  field (ZNAM, "FALSE")
  field (ONAM, "TRUE")
  field (INP, "@$(PLC)/$(INOFF)+8 T=INT8 B=0")
  info(ARCHIVE_THIS, "")
}

#- Get real measurement from PLC analog input module
record (ai, "$(P)$(R)-Val") {
  field(DESC, "$(DESC) Value")
  field(EGU,  "ºC")
  field(PREC, "1")
  field(ASLO, "0.1")
  field(SCAN, "I/O Intr")
  field(DTYP, "S7plc")
  field(INP, "@$(PLC)/$(INOFF)+10 T=WORD")
  info(ARCHIVE_THIS, "")
}

#- Binary readback of LOW WARNING 
record (bi, "$(P)$(R)-LowAlarmStat") {
  field (DESC, "$(DESC) Low Warning")
  field (DTYP, "S7plc")
  field (SCAN, "I/O Intr")
  field (PINI, "YES")
  field (ZNAM, "FALSE")
  field (ONAM, "TRUE")
  field (INP, "@$(PLC)/$(INOFF)+12 T=INT8 B=$(BI0=0)")
  info(ARCHIVE_THIS, "")
}

#- Binary readback of HIGH WARNING 
record (bi, "$(P)$(R)-HighAlarmStat") {
  field (DESC, "$(DESC) High Warning")
  field (DTYP, "S7plc")
  field (SCAN, "I/O Intr")
  field (PINI, "YES")
  field (ZNAM, "FALSE")
  field (ONAM, "TRUE")
  field (INP, "@$(PLC)/$(INOFF)+12 T=INT8 B=$(BI1=1)")
  info(ARCHIVE_THIS, "")
}

#- Binary readback of LOW ALARM 
record (bi, "$(P)$(R)-LoloAlarmStat") {
  field (DESC, "$(DESC) Low Alarm")
  field (DTYP, "S7plc")
  field (SCAN, "I/O Intr")
  field (PINI, "YES")
  field (ZNAM, "FALSE")
  field (ONAM, "TRUE")
  field (INP, "@$(PLC)/$(INOFF)+12 T=INT8 B=$(BI2=2)")
  info(ARCHIVE_THIS, "")
}

#- Binary readback of HIGH ALARM 
record (bi, "$(P)$(R)-HihiAlarmStat") {
  field (DESC, "$(DESC) High Alarm")
  field (DTYP, "S7plc")
  field (SCAN, "I/O Intr")
  field (PINI, "YES")
  field (ZNAM, "FALSE")
  field (ONAM, "TRUE")
  field (INP, "@$(PLC)/$(INOFF)+12 T=INT8 B=$(BI3=3)")
  info(ARCHIVE_THIS, "")
}

#- Binary readback of general (sum) alarms 
record (bi, "$(P)$(R)-AlarmStat") {
  field (DESC, "$(DESC) Alarm")
  field (DTYP, "S7plc")
  field (SCAN, "I/O Intr")
  field (PINI, "YES")
  field (ZNAM, "FALSE")
  field (ONAM, "TRUE")
  field (INP, "@$(PLC)/$(INOFF)+12 T=INT8 B=$(BI4=4)")
  info(ARCHIVE_THIS, "")
}
