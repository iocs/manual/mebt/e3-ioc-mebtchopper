require stream
require essioc
require iocmetadata
require s7plc

#- MEBT-010:BMD-Chop-001
epicsEnvSet("SYSSUB",    "MEBT-010")
epicsEnvSet("CHPDEV",    "BMD-Chop-001")
epicsEnvSet("PLCDEV",    "BMD-PLC-012")
epicsEnvSet("PULPOSDEV", "PwrC-PSChopP-001")
epicsEnvSet("PULNEGDEV", "PwrC-PSChopN-001")

epicsEnvSet("STREAM_PROTOCOL_PATH","$(E3_CMD_TOP)/db")
epicsEnvSet("ENGINEER", "JoaoPaulo Martins <joaopaulo.martins@ess.eu>"

#- Positive Load - Flow Switch and Temperature (PT-100)
epicsEnvSet("PLOAD_FS", "BMD-FS-012")
epicsEnvSet("PLOAD_TE", "BMD-TE-013")

#- Negative Load - Flow Switch and Temperature (PT-100)
epicsEnvSet("NLOAD_FS", "BMD-FS-013")
epicsEnvSet("NLOAD_TE", "BMD-TE-014")

#- Beam Dump - Flow Switch and Temperature (PT-100)
epicsEnvSet("DUMP_FS", "BMD-FS-014")
epicsEnvSet("DUMP_TE", "BMD-TE-001")

#- 24V Supply Status
epicsEnvSet("STATUS_24V", "BMD-PS-012")

#- Choper vessel temperature sensor: Beam Out Bottom 
epicsEnvSet("OUTBOT_TE", "BMD-TE-015")

#- Choper vessel temperature sensor: Beam Out Top 
epicsEnvSet("OUTTOP_TE", "BMD-TE-016")

#- Choper vessel temperature sensor: Beam In Top 
epicsEnvSet("INTOP_TE", "BMD-TE-017")

#- Choper vessel temperature sensor: Beam In Bottom 
epicsEnvSet("INBOT_TE", "BMD-TE-018")

#- HV Pulsers Power Supply Control 
epicsEnvSet("MOXA_IP", "mebt-chopper-moxa.tn.esss.lu.se")
epicsEnvSet("POSPUL_PORT", "4001")
epicsEnvSet("NEGPUL_PORT", "4002")

#- Record (from PLC) that reflects overall HEALTH status (MPS output)
epicsEnvSet("MPS_HEALTH", "$(SYSSUB):$(CHPDEV):Health")
#epicsEnvSet("MPS_HEALTH", "$(SYSSUB):$(CHPDEV):Health-Sim")

#- Vacuum status used to interlock in case of fault
epicsEnvSet("VAC_HEALTH", "MEBT-010:Vac-VGC-10000:Rly4_StatR")
#epicsEnvSet("VAC_HEALTH", "$(SYSSUB):$(CHPDEV):Vacuum-Sim")

#- EVR PVs (turn on/off triggers)
epicsEnvSet("EVRDEV", "$(SYSSUB):Ctrl-EVR-001")
epicsEnvSet("EVRDEV_OUT1","$(EVRDEV):Out-FPUV0-Src-SP")
epicsEnvSet("EVRDEV_OUT2","$(EVRDEV):Out-FPUV1-Src-SP")
epicsEnvSet("EVRDEV_SP","12")

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet("LOG_SERVER_NAME", "172.16.107.59")
# epicsEnvSet("AS_TOP", "/opt/nonvolatile")
# epicsEnvSet("IOCDIR", "MEBT-010_SC-IOC-001")
# epicsEnvSet("IOCNAME", "MEBT-010:SC-IOC-001")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

#- Load main snippet (PLC connection and database records)
iocshLoad("$(E3_CMD_TOP)/iocsh/mebtchopper_main.iocsh")

#- Create NTTable PV with list of PVs to be archived
pvlistFromInfo("ARCHIVE_THIS", "MEBT-010:SC-IOC-001:ArchiverList")

#- Start IOC
iocInit()

